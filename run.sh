#!/bin/bash

mysqld --verbose --skip-grant-tables --disable-partition-engine-check --innodb_default_row_format=DYNAMIC --user=mysql --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib/x86_64-linux-gnu/mariadb18/plugin --user=mysql --skip-log-error --pid-file=/var/run/mysqld/mysqld.pid --socket=/var/run/mysqld/mysqld.sock --port=3306 &
apache2ctl -D FOREGROUND
