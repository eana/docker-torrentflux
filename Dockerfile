FROM debian:stretch-slim

ENV DEBIAN_FRONTEND noninteractive

# Add the script that will start mysql and apache
# and my awesome personal patch
ADD run.sh sync.patch /

# Install HTTPD and GIT && Get latest Lavern Minifield
RUN set -xe && \
    apt-get update && \
    apt-get install -y --no-install-recommends wget ca-certificates gnupg2 && \
    echo "deb http://deb.debian.org/debian wheezy main" >> /etc/apt/sources.list && \
    echo "deb http://repo.mysql.com/apt/debian/ stretch mysql-5.7\ndeb-src http://repo.mysql.com/apt/debian/ stretch mysql-5.7" > /etc/apt/sources.list.d/mysql.list && \
    wget -O /tmp/RPM-GPG-KEY-mysql https://repo.mysql.com/RPM-GPG-KEY-mysql && \
    apt-key add /tmp/RPM-GPG-KEY-mysql && \
    apt-get update && \
    apt-get install -y vim patch torrentflux && \
    /etc/init.d/mysql start && \
    mysql -u root -Bsqe "SET GLOBAL innodb_default_row_format=DYNAMIC" && \
    mysql -u root -Bsqe "CREATE DATABASE torrentflux" && \
    mysql -u root torrentflux < /usr/share/dbconfig-common/data/torrentflux/install/mysql && \
    patch -t -p0 < /sync.patch && \
    rm -f /sync.patch /torrentflux-schema.sql && \
    rm -f /etc/apache2/sites-enabled/000-default && \
    apt-get autoremove --purge -y wget ca-certificates gnupg2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    chmod u+x /run.sh

# add database configuration for torrentflux
ADD config-db.php /etc/torrentflux/config-db.php

# add the apache configuration
ADD apache.conf /etc/apache2/sites-enabled/torrentflux.conf

EXPOSE 80

# CMD ["apache2ctl", "-D", "FOREGROUND"]

# the data will be stored under /data/torrents
# so, mount it externally to be able to access your files from the host OS
# otherwise the data will only be available from the torrentflux app
# docker run -d -p 8080:80 --name torrentflux -v /data/torrents:/data/torrents torrentflux

CMD ["/run.sh"]
