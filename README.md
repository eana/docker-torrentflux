docker-torrentflux
==================

Docker builder file for torrentflux (mysql is includes as part of the image/container)

#### Clone and build

```
git clone https://github.com/eana/docker-torrentflux.git
cd docker-torrentflux.git
docker build -t torrentflux .
```

### Run

```
docker run --name torrentflux -d -p 8080:80 -p "49160-49300:49160-49300" -v /path/to/.well-known:/usr/share/torrentflux/www/.well-known-v /path/to/torrents:/var/cache/torrentflux torrentflux
```

* `--name <name>`: name the container
* `-d`: runs as daemon (background)
* `-p 8080:80`: redirects connections arriving on port 8080 of the host OS to port 80 of the container
* `-p "49160-49300:49160-49300": expose high ports for incoming connections
* `-v /path/to/torrents:/var/cache/torrentflux`: mount the folder where torrents will be downloaded to in the host OS

### Features

* Torrentflux is available from `http://<host IP>/` (default username/password: admin/admin)
